//
//  Constants.swift
//  RoverTaxiUser
//
//  Created by Prashant on 08/08/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import Foundation
import UIKit


struct Constants {
    
    static let SCREEN_WIDTH  = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    
    static let CORNER_RADIUS_3:CGFloat = 3.0
    static let CORNER_RADIUS_5:CGFloat = 5.0

    
    // CELL ID'S
    static let CELL_MORE_VIDEO  = "MoreVideo"
    
    
    
    // SEGUES
    
    
    
    
    
    // STRINGS
    static let NO_INTERNET  = "Please check your internet connection."
    
    
    //trip status
  
  
    
    //side menu
  
    
    //storyboard id's
    static let STORYBOARD_ID_PRIVACY        = "PrivacyViewController"
    static let STORYBOARD_ID_SETTINGS       = "SettingsViewController"
    
    
    
    
    
   
}


/*
 public class func openSansFontOfSize(_ size: Float) -> UIFont! {
 return UIFont(name: "OpenSans", size: makeSize(size))
 }

 */

// MARK: - Font

struct Font {
    
    //let font = Font.header.withSize(18.0)
    
    static let OPENSAN_REGULAR  = UIFont(name: "Montserrat-Regular", size: 14.0)!
    static let OPENSAN_LIGHT    = UIFont(name: "Montserrat-Light", size: 14.0)!
    
    static let NAV_HEADER = UIFont(name: "Montserrat-Light", size: 14.0)!
    
    static func ROBOTO_REGULAR(size: CGFloat) -> UIFont {
        return UIFont(name: "Regular", size: size)!
    }
    
    /*
     Montserrat-Regular
     Montserrat-Light
     */
}



struct BLE {
    
    static let ENABLE_ON        = "1"
    static let ENABLE_OFF       = "0"
}


























