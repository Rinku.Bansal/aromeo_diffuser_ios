//
//  Utility.swift
//  Saud
//
//  Created by vinove on 28/07/17.
//  Copyright © 2017 Munendra. All rights reserved.
//

import Foundation
import UIKit
//import MBProgressHUD

class Utility {
    public static let main = Utility()
    //fileprivate var hud:MBProgressHUD?
    fileprivate var screenWidth = UIScreen.main.bounds.size.width
    fileprivate var screenHeight = UIScreen.main.bounds.size.height
    
    var appDelegate:AppDelegate {
        get{
            return UIApplication.shared.delegate as! AppDelegate
        }
    }
    
    var keyWindow:UIWindow {
        get{
            return UIApplication.shared.keyWindow!
        }
    }
    /*
    var isHudShow:Bool = {
        guard let view = UIApplication.shared.keyWindow?.viewWithTag(1) else{
            return false
        }
        if view.isKind(of: MBProgressHUD.self){
            return true
        }else{
            return false
        }
    }()
    */
    lazy var isIPhone:Bool = {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return false
        }else{
            return true
        }
    }()
    
    //MARK:- IPHONE -
    lazy var isIphone_7P_6P:Bool = {
        if self.screenHeight == 736{
            return true
        }else{
            return false
        }
    }()
    
    lazy var isIphone_7_6:Bool = {
        if self.screenHeight == 667{
            return true
        }else{
            return false
        }
    }()
    lazy var isIphone_5:Bool = {
        if self.screenHeight == 568{
            return true
        }else{
            return false
        }
    }()
    lazy var isIphoneX:Bool = {
        if self.screenHeight == 812{
            return true
        }else{
            return false
        }
    }()
    private init(){
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
// MARK: - HUD -
extension Utility{
    /*
    func showHud(message:String?) {
        if isHudShow {
            keyWindow.viewWithTag(1)?.removeFromSuperview()
        }
        hud = MBProgressHUD.showAdded(to: keyWindow, animated: true)
        hud?.tag = 1
        if let msg = message {
               hud?.label.text = msg
        }else{
              hud?.label.text = "Loading..."
        }
    }
    
    func hideHud() {
        // hud?.hide(animated: true)
        guard let tempHud = hud else {
            return
        }
        tempHud.removeFromSuperview()
    }
    */
    func showAlert(title:String?, message:String,controller:UIViewController) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title ?? "" , message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action) in
                
            }))
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
}

