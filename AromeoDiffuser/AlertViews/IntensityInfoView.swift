//
//  IntensityInfoView.swift
//  AromeoDiffuser
//
//  Created by vinove on 15/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class IntensityInfoView: UIView {

    @IBOutlet weak var viewBack: UIView!
    var nibView: UIView!
    
    func displayIntensityInfoView() -> UIView {
        return UINib(nibName: "IntensityInfoView", bundle: Bundle.main).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(viewTapped))
        viewBack.isUserInteractionEnabled = true
        viewBack.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        nibView = self.displayIntensityInfoView()
        nibView.frame = bounds
        nibView.autoresizingMask = [UIViewAutoresizing.flexibleWidth ,UIViewAutoresizing.flexibleHeight]
        addSubview(nibView)
    }
    
    @objc func viewTapped(){
        self.removeFromSuperview()
        
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func btnGet(_ sender: Any) {
        self.removeFromSuperview()
    }
    
}
