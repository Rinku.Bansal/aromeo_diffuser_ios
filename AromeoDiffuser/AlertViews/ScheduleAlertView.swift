//
//  ScheduleAlertView.swift
//  AromeoDiffuser
//
//  Created by vinove on 15/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

protocol quitSelectScheduleProtocol {
    func quitSelectedScheduleVal()
}

class ScheduleAlertView: UIView {

    @IBOutlet weak var lblAlert: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var imgType: UIImageView!
    
    var nibView: UIView!
    var quitScheduleDelagate: quitSelectScheduleProtocol?
    
    func displayAlertView() -> UIView {
        return UINib(nibName: "ScheduleAlertView", bundle: Bundle.main).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(viewTapped))
        backView.isUserInteractionEnabled = true
        backView.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        nibView = self.displayAlertView()
        nibView.frame = bounds
        nibView.autoresizingMask = [UIViewAutoresizing.flexibleWidth ,UIViewAutoresizing.flexibleHeight]
        addSubview(nibView)
    }
    
    @objc func viewTapped(){
        self.removeFromSuperview()
        
    }
    
    @IBAction func btnNo(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func btnYes(_ sender: Any) {
        self.removeFromSuperview()
        if quitScheduleDelagate != nil {
            quitScheduleDelagate?.quitSelectedScheduleVal()
        }
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.removeFromSuperview()
    }
    
}
