//
//  GuideCell.swift
//  AromeoDiffuser
//
//  Created by Prashant on 12/10/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class GuideCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbl: UILabel!
}
