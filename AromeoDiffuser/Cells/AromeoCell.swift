//
//  AromeoCell.swift
//  AromeoDiffuser
//
//  Created by Prashant on 12/10/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit


class AromeoCell: UITableViewCell {

    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewRefresh: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    
    var device:Device?
    /*{
        didSet{
            
            if device?.type == DeviceType.Refresh {
                self.viewBack.setShadow(toRemove: true)
                self.viewRefresh.isHidden = false
                self.lblName.isHidden = true
                self.viewBack.backgroundColor = .clear
            }
            else {
                self.viewBack.backgroundColor = .white
                self.viewBack.setShadow(toRemove: false)
                self.viewRefresh.isHidden = true
                self.lblName.isHidden = false
                if device?.peripheral == nil {
                    self.lblName.text = "AROMOAlpha002"
                }else {
                    self.lblName.text = device?.peripheral?.name
                }
            }
        }
    }*/
    
    func setData(device:Device){
        
        //self.device = device
        
        if device.type == DeviceType.Refresh {
            self.viewBack.setShadow(toRemove: true)
            self.viewRefresh.isHidden = false
            self.lblName.isHidden = true
            self.viewBack.backgroundColor = .clear
        }
        else {
            self.viewBack.backgroundColor = .white
            self.viewBack.setShadow(toRemove: false)
            self.viewRefresh.isHidden = true
            self.lblName.isHidden = false
            
            
            if let name = device.peripheral?.name {
                
                if name == "MyESP32" {
                    self.lblName.text = "Aromeo - \(name)"
                }
                else {
                     self.lblName.text = name
                }
            }else {
                self.lblName.text = "Unknown Device"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBack.setShadow(withCorners: self.viewBack.frame.size.height/2.0)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
