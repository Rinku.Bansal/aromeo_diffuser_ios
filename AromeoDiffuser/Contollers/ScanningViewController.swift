//
//  ScanningViewController.swift
//  AromeoDiffuser
//
//  Created by Prashant on 12/10/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit
import Foundation
import CoreBluetooth


var txCharacteristic : CBCharacteristic?
var rxCharacteristic : CBCharacteristic?
var blePeripheral : CBPeripheral?
var characteristicASCIIValue = NSString()


//test
var synCharacteristic : CBCharacteristic?

struct Device {
    //var name:String?
    var type:DeviceType
    var peripheral:CBPeripheral?
}

enum DeviceType {
    case IOT
    case Refresh
}

enum CBType {
    case Synchronize
    case Schedule
    case Enable
}


class ScanningViewController: DiffuserViewController {

    
    @IBOutlet weak var viewNav: UIView!
  
    @IBOutlet weak var viewStartSearch: UIView!
    
    //view Guide
    @IBOutlet weak var viewGuide: UIView!
    @IBOutlet weak var collectionviewGuide: UICollectionView!
    @IBOutlet weak var pagecontrol: UIPageControl!
    @IBOutlet weak var btnSearchAgain: UIButton!
    
    //view Searched Devices
    @IBOutlet weak var viewSearchedDevices: UIView!
    @IBOutlet weak var tblDevices: UITableView!
    
    
    // view tap bar
    @IBOutlet weak var viewTapbar: UIView!
    @IBOutlet var collectionBtnTapBar: [UIButton]!
    @IBOutlet var collectionBorderTapBar: [UIView]!
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    
    // MARK: - Private
    
    //Data
    private var centralManager : CBCentralManager!
    private var RSSIs = [NSNumber]()
    private var data = NSMutableData()
    private var writeData: String = ""
    var peripherals: [Device] = []    //[CBPeripheral]
    private var characteristicValue = [CBUUID: NSData]()
    private var timer = Timer()
    private var characteristics = [String : CBCharacteristic]()
    var isBluetoothStatus : Bool = false
    private var guideTimer = Timer()

    
    
    //Guide
    let arrGuide = [["home_guide1":"1.Turn on Aromeo Diffuser"],
                    ["home_guide2": "2. When diffuser is not connected to power, long press top button for 3 seconds until top light is on."]]
    
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        centralManager = CBCentralManager(delegate: self, queue: nil)
        
        self.peripherals = []
        self.btnSearchAgain.addRoundCorners(cornerRadius: self.btnSearchAgain.frame.size.height/2.0)
        self.viewTapbar.setShadow(toRemove: false)
        
        self.viewGuide.isHidden = true
        self.viewSearchedDevices.isHidden = true
        self.viewStartSearch.isHidden = false
        
        self.handleBtns_TapBar(self.collectionBtnTapBar[0])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("Stop Scanning")
        stopScan()
    }

    
    

    //MARK: - Handle Btn Events
    
    @IBAction func handleBtnInfo(_ sender: UIButton) {
        guideTimer.invalidate()
        self.timer.invalidate()
        let control = self.storyboard?.instantiateViewController(withIdentifier: "GuideViewController") as! GuideViewController
        self.present(control, animated: true, completion: nil)

    }
    
    @IBAction func handleBtnSearchAgain(_ sender: UIButton) {
        if !isBluetoothStatus {
            self.showBluetoothAlert()
            return
        }
        self.viewGuide.isHidden = true
        self.viewSearchedDevices.isHidden = true
        self.viewStartSearch.isHidden = false
        self.startScan()
    }
    
    
    
    @IBAction func handleBtns_TapBar(_ sender: UIButton) {
        
        for btn in self.collectionBtnTapBar {
            btn.isSelected = btn.tag == sender.tag ? true : false
        }
        for view in self.collectionBorderTapBar {
            view.isHidden = view.tag == sender.tag ? false : true
        }
        
        if sender.tag == 1 {
            self.writeValue(data: BLE.ENABLE_ON, type: CBType.Enable)
        }else if sender.tag == 2{
            //self.writeValue(data: BLE.ENABLE_OFF)
            self.writeValue(data: "2018-11-12T17:05:00", type: CBType.Synchronize)
        }else {
            //self.writeValue(data: "0", type: CBType.Schedule)
            let control = self.storyboard?.instantiateViewController(withIdentifier: "CreateScheduleViewController") as! CreateScheduleViewController
            self.present(control, animated: true, completion: nil)
        }
        

    }
    
    
    //MARK:- Helper Methods
    func showBluetoothAlert(){
        Utility.main.showAlert(title: "Bluetooth is not enabled", message: "Make sure that your bluetooth is turned on", controller: self)
    }
    
    func manageTapBarState(){
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}









// MARK: - Extension for Bluetooth Controlls

extension ScanningViewController {
    
    /**
     CBCentalManager is up and running, it's time to start searching for devices
     */
    func startScan() {
        peripherals = []
        let device = Device(type: DeviceType.Refresh, peripheral: nil)
        self.peripherals.insert(device, at: 0)
       // let device = Device(type: DeviceType.IOT, peripheral: nil)
       // peripherals.append(device)
        print("Now Scanning...")
        self.timer.invalidate()
        //centralManager?.scanForPeripherals(withServices: [BLEService_UUID] , options: [CBCentralManagerScanOptionAllowDuplicatesKey:false])
        centralManager?.scanForPeripherals(withServices: nil , options: nil)
        Timer.scheduledTimer(timeInterval: 17, target: self, selector: #selector(self.stopScan), userInfo: nil, repeats: false)
    }
    
    @objc func stopScan(){
        centralManager?.stopScan()
        var isDiffusr: Bool = false
        for device in self.peripherals {
            let diffuserDevice: Device = device
            if diffuserDevice.peripheral?.name == "MyESP32" {
                isDiffusr = true
            }
        }
        if !isDiffusr {
            guideTimer = Timer.scheduledTimer(timeInterval: 9, target: self, selector: #selector(self.showGuideView), userInfo: nil, repeats: true)
        }
    }
    
    
    func refreshAction() {
        disconnectFromDevice()
        self.peripherals = []
        self.RSSIs = []
        
        self.tblDevices.reloadData()
        startScan()
    }
    
    //-Terminate all Peripheral Connection
    /*
     Call this when things either go wrong, or you're done with the connection.
     This cancels any subscriptions if there are any, or straight disconnects if not.
     (didUpdateNotificationStateForCharacteristic will cancel the connection if a subscription is involved)
     */
    func disconnectFromDevice () {
        if blePeripheral != nil {
            // We have a connection to the device but we are not subscribed to the Transfer Characteristic for some reason.
            // Therefore, we will just disconnect from the peripheral
            centralManager?.cancelPeripheralConnection(blePeripheral!)
        }
    }
    
    
    func restoreCentralManager() {
        //Restores Central Manager delegate if something went wrong
        centralManager?.delegate = self
    }
    
    func disconnectAllConnection() {
        centralManager.cancelPeripheralConnection(blePeripheral!)
    }
    
    // Show Guide View when not getting the Aromeo - Diffuser Device
    @objc func showGuideView() {
        if guideTimer.timeInterval > 8.0 {
            guideTimer.invalidate()
            self.viewGuide.isHidden = false
            self.viewStartSearch.isHidden = true
            self.viewSearchedDevices.isHidden = true
        }
    }
}



//MARK:- Writing Vaules to Device
extension ScanningViewController {
    
    // Write functions
    func writeValue(data: String, type: CBType){
        let valueString = (data as NSString).data(using: String.Encoding.utf8.rawValue)
        //change the "data" to valueString
        if let blePeripheral = blePeripheral{
            switch type {
            case CBType.Enable:
                 blePeripheral.writeValue(valueString!, for: rxCharacteristic!, type: CBCharacteristicWriteType.withResponse)
                break
            case .Synchronize:
                 blePeripheral.writeValue(valueString!, for: synCharacteristic!, type: CBCharacteristicWriteType.withResponse)
                break
            case .Schedule:
                blePeripheral.writeValue(valueString!, for: txCharacteristic!, type: CBCharacteristicWriteType.withResponse)
                break
            }
          //  if let txCharacteristic = txCharacteristic {
          //  blePeripheral.writeValue(valueString!, for: synCharacteristic!, type: CBCharacteristicWriteType.withResponse)
//            }else if let txCharacteristic = rxCharacteristic {
//                blePeripheral.writeValue(valueString!, for: txCharacteristic, type: CBCharacteristicWriteType.withResponse)
//            }else if let txCharacteristic = synCharacteristic {
//                blePeripheral.writeValue(valueString!, for: txCharacteristic, type: CBCharacteristicWriteType.withResponse)
//            }
        }
    }
    
    func writeCharacteristic(val: Int8){
        var val = val
        let ns = NSData(bytes: &val, length: MemoryLayout<Int8>.size)
        blePeripheral!.writeValue(ns as Data, for: txCharacteristic!, type: CBCharacteristicWriteType.withResponse)
    }
    
    
}



extension ScanningViewController : CBCentralManagerDelegate, CBPeripheralDelegate {
    
    /*
     Invoked when the central manager’s state is updated.
     This is where we kick off the scan if Bluetooth is turned on.
     */
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == CBManagerState.poweredOn {
            // We will just handle it the easy way here: if Bluetooth is on, proceed...start scan!
            print("Bluetooth Enabled")
            isBluetoothStatus = true
            startScan()
            self.viewGuide.isHidden = true
            self.viewSearchedDevices.isHidden = true
            self.viewStartSearch.isHidden = false
            
        } else {
            //If Bluetooth is off
            //print("Bluetooth Disabled- Make sure your Bluetooth is turned on")
            isBluetoothStatus = false
            self.showBluetoothAlert()
            
            peripherals = []
            let device = Device(type: DeviceType.Refresh, peripheral: nil)
            peripherals.append(device)
            self.tblDevices.reloadData()
        }
        
    }
    
    /*
     Called when the central manager discovers a peripheral while scanning. Also, once peripheral is connected, cancel scanning.
     */
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        print(peripheral)
        //blePeripheral = peripheral
        let device = Device(type: DeviceType.IOT, peripheral: peripheral)
        self.peripherals.insert(device, at: 0)
        //self.peripherals.append(device)
        self.RSSIs.append(RSSI)
        peripheral.delegate = self //as? CBPeripheralDelegate
        
        if self.peripherals.count > 0 {
            self.viewStartSearch.isHidden = true
            self.viewGuide.isHidden = true
            self.viewSearchedDevices.isHidden = false
        }
       
        self.tblDevices.reloadData()
        
        if blePeripheral == nil {
            print("Found new pheripheral devices with services")
            print("Peripheral name: \(String(describing: peripheral.name))")
            print("**********************************")
            print ("Advertisement Data : \(advertisementData)")
        }
    }
    
    func connect(_ peripheral: CBPeripheral, options: [String : Any]? = nil) {
        self.activityIndicator.startAnimating()
        self.disconnectFromDevice()
        centralManager?.connect(peripheral, options: nil)
    }
    
    
    //-Connected
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {

        self.activityIndicator.stopAnimating()
        
        Utility.main.showAlert(title: nil, message: "\(peripheral.name!) is connected", controller: self)
        
        blePeripheral = peripheral
        print("*****************************")
        print("Connection complete")
        print("Peripheral info: \(String(describing: blePeripheral))")
        
        //Stop Scan- We don't need to scan once we've connected to a peripheral. We got what we came for.
        centralManager?.stopScan()
        print("Scan Stopped")
        
        //Erase data that we might have
        data.length = 0
        
        //Discovery callback
        peripheral.delegate = self //as? CBPeripheralDelegate
        //Only look for services that matches transmit uuid
        peripheral.discoverServices([BLEService_UUID])
        
        
        /*
         //Once connected, move to new view controller to manager incoming and outgoing data
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         
         let uartViewController = storyboard.instantiateViewController(withIdentifier: "UartModuleViewController") as! UartModuleViewController
         
         uartViewController.peripheral = peripheral
         
         navigationController?.pushViewController(uartViewController, animated: true)
         */
    }
    
    
    
    /*
     Invoked when the central manager fails to create a connection with a peripheral.
     */
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        if error != nil {
            print("Failed to connect to peripheral")
            self.activityIndicator.stopAnimating()
            return
        }
    }
    
    
    
    
    /*
     Invoked when you discover the peripheral’s available services.
     This method is invoked when your app calls the discoverServices(_:) method. If the services of the peripheral are successfully discovered, you can access them through the peripheral’s services property. If successful, the error parameter is nil. If unsuccessful, the error parameter returns the cause of the failure.
     */
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("*******************************************************")
        
        if ((error) != nil) {
            print("Error discovering services: \(error!.localizedDescription)")
            return
        }
        
        guard let services = peripheral.services else {
            return
        }
        //We need to discover the all characteristic
        for service in services {
            
            peripheral.discoverCharacteristics(nil, for: service)
            // bleService = service
        }
        print("Discovered Services: \(services)")
    }
    
    /*
     Invoked when you discover the characteristics of a specified service.
     This method is invoked when your app calls the discoverCharacteristics(_:for:) method. If the characteristics of the specified service are successfully discovered, you can access them through the service's characteristics property. If successful, the error parameter is nil. If unsuccessful, the error parameter returns the cause of the failure.
     */
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        print("*******************************************************")
        
        if ((error) != nil) {
            print("Error discovering services: \(error!.localizedDescription)")
            return
        }
        
        guard let characteristics = service.characteristics else {
            return
        }
        
        print("Found \(characteristics.count) characteristics!")
        
        for characteristic in characteristics {
            //looks for the right characteristic
            
            if characteristic.uuid.isEqual(BLE_Characteristic_uuid_State)  {
                rxCharacteristic = characteristic
                
                //Once found, subscribe to this particular characteristic...
                peripheral.setNotifyValue(true, for: rxCharacteristic!)
                // We can return after calling CBPeripheral.setNotifyValue because CBPeripheralDelegate's
                // didUpdateNotificationStateForCharacteristic method will be called automatically
                peripheral.readValue(for: characteristic)
                print("Rx Characteristic: \(characteristic.uuid)")
            }
            if characteristic.uuid.isEqual(BLE_Characteristic_uuid_Schedule){
                txCharacteristic = characteristic
                peripheral.readValue(for: characteristic)
                print("Tx Characteristic: \(characteristic.uuid)")
            }
            
            if characteristic.uuid.isEqual(BLE_Characteristic_uuid_Syncronize){
                synCharacteristic = characteristic
                peripheral.readValue(for: characteristic)
                print("synchronize Characteristic: \(characteristic.properties)")
            }
            
            // Leave
           // peripheral.discoverDescriptors(for: characteristic)
        }
    }
    
    // Getting Values From Characteristic
    
    /*After you've found a characteristic of a service that you are interested in, you can read the characteristic's value by calling the peripheral "readValueForCharacteristic" method within the "didDiscoverCharacteristicsFor service" delegate.
     */
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        if characteristic == rxCharacteristic {
            if let ASCIIstring = NSString(data: characteristic.value!, encoding: String.Encoding.utf8.rawValue) {
                characteristicASCIIValue = ASCIIstring
                print("Value Recieved1: \((characteristicASCIIValue as String))")
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "Notify"), object: nil)
                
            }
        }
        if characteristic == txCharacteristic {
            if let ASCIIstring = NSString(data: characteristic.value!, encoding: String.Encoding.utf8.rawValue) {
                characteristicASCIIValue = ASCIIstring
                print("Value Recieved2: \((characteristicASCIIValue as String))")
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "Notify"), object: nil)
                
            }
        }
        
        if characteristic == synCharacteristic {
            if let ASCIIstring = NSString(data: characteristic.value!, encoding: String.Encoding.utf8.rawValue) {
                characteristicASCIIValue = ASCIIstring
                print("Value Recieved3: \((characteristicASCIIValue as String))")
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "Notify"), object: nil)
                
            }
        }
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        print("*******************************************************")
        
        if error != nil {
            print("\(error.debugDescription)")
            return
        }
        if ((characteristic.descriptors) != nil) {
            
            for x in characteristic.descriptors!{
                let descript = x as CBDescriptor?
                print("function name: DidDiscoverDescriptorForChar \(String(describing: descript?.description))")
                print("Rx Value \(String(describing: rxCharacteristic?.value))")
                print("Tx Value \(String(describing: txCharacteristic?.value))")
            }
        }
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        print("*******************************************************")
        
        if (error != nil) {
            print("Error changing notification state:\(String(describing: error?.localizedDescription))")
            
        } else {
            print("Characteristic's value subscribed")
        }
        
        if (characteristic.isNotifying) {
            print ("Subscribed. Notification has begun for: \(characteristic.uuid)")
        }
    }
    
    
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("Disconnected")
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        guard error == nil else {
            print("Error discovering services: error")
            return
        }
        print("Message sent")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        guard error == nil else {
            print("Error discovering services: error")
            return
        }
        print("Succeeded!")
    }
    
    
}























