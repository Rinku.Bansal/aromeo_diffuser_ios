//
//  DiffusionTimeTableViewCell.swift
//  AromeoDiffuser
//
//  Created by vinove on 15/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class DiffusionTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
