//
//  DeviceSettingViewController.swift
//  AromeoDiffuser
//
//  Created by vinove on 15/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class DeviceSettingViewController: DiffuserViewController, quitSelectScheduleProtocol, selectDeffusionProtocol {

    @IBOutlet weak var lblAromeoId: UILabel!
    @IBOutlet weak var toggleMoodLight: UISwitch!
    @IBOutlet weak var lblDefaultDiffusion: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toggleMoodLight(_ sender: Any) {
        
    }
    
    @IBAction func btnDisconnect(_ sender: Any) {
        let window = UIApplication.shared.keyWindow
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        let viewPopUp = ScheduleAlertView(frame: frame)
        viewPopUp.btnNo .setTitle("Cancel", for: .normal)
        viewPopUp.btnYes .setTitle("Disconnect", for: .normal)
        viewPopUp.imgType.image = UIImage (named: "icDisconnect72")
        viewPopUp.quitScheduleDelagate = self
        window?.addSubview(viewPopUp)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSelectDefaultDiffusion(_ sender: Any) {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "DiffusingButtonViewController") as! DiffusingButtonViewController
        control.selectDiffusionDelegate = self
        self.present(control, animated: true, completion: nil)
    }
    
    
    func quitSelectedScheduleVal() {
        
    }
    
    func selectDiffusionButton(time: String) {
        lblDefaultDiffusion.text = time
    }
    
}
