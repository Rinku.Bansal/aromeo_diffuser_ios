//
//  DiffusingButtonViewController.swift
//  AromeoDiffuser
//
//  Created by vinove on 15/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

protocol selectDeffusionProtocol {
    func selectDiffusionButton(time: String)
}

class DiffusingButtonViewController: DiffuserViewController {

    @IBOutlet weak var diffusionTableView: UITableView!
    let timeArr = ["15 min", "20 min", "25 min", "30 min", "35 min", "40 min", "45 min", "50 min"]
    var selectDiffusionDelegate: selectDeffusionProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        diffusionTableView.reloadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension DiffusingButtonViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "DiffusionTimeTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! DiffusionTimeTableViewCell
        cell.lblTime.text = timeArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:DiffusionTimeTableViewCell = tableView.cellForRow(at: indexPath as IndexPath)! as! DiffusionTimeTableViewCell
        selectedCell.containerView.backgroundColor = UIColor (red: 111.0/255.0, green: 128.0/255.0, blue: 196.0/255.0, alpha: 1)
        selectedCell.lblTime.textColor = UIColor.white
        if selectDiffusionDelegate != nil {
            self.dismiss(animated: true, completion: nil)
            selectDiffusionDelegate?.selectDiffusionButton(time: selectedCell.lblTime.text!)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cellToDeSelect:DiffusionTimeTableViewCell = tableView.cellForRow(at: indexPath as IndexPath)! as! DiffusionTimeTableViewCell
        cellToDeSelect.containerView.backgroundColor = UIColor.clear
        cellToDeSelect.lblTime.textColor = UIColor (red: 88.0/255.0, green: 102.0/255.0, blue: 156.0/255.0, alpha: 1)
    }
}
