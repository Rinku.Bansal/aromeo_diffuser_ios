//
//  DurationTableViewCell.swift
//  AromeoDiffuser
//
//  Created by vinove on 13/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

protocol changeDurationProtocol {
    func changeDurationWith(duration: String)
}

class DurationTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var durationPickerView: UIPickerView!
    @IBOutlet weak var timeContainerView: UIView!
    var changeDurationDelegate: changeDurationProtocol?
    let arrTime = ["10 Min", "15 Min", "20 Min", "25 Min", "30 Min", "35 Min", "40 Min",]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.durationPickerView.delegate = self
        self.durationPickerView.dataSource = self
        self.durationPickerView.reloadAllComponents()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}


extension DurationTableViewCell: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrTime.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrTime[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        lblTime.text = arrTime[row]
        if changeDurationDelegate != nil {
            changeDurationDelegate?.changeDurationWith(duration: arrTime[row])
        }
    }
}
