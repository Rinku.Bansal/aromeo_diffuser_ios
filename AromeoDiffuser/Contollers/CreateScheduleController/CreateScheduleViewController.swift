//
//  CreateScheduleViewController.swift
//  AromeoDiffuser
//
//  Created by vinove on 12/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class CreateScheduleViewController: DiffuserViewController, quitSelectScheduleProtocol {
    
    var isSelectSchedule: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- @IBAction
    @IBAction func btnBack(_ sender: Any) {
        if isSelectSchedule {
            let window = UIApplication.shared.keyWindow
            let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            let viewPopUp = ScheduleAlertView(frame: frame)
            viewPopUp.quitScheduleDelagate = self
            window?.addSubview(viewPopUp)
        }else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- quitSelectScheduleProtocol
    func quitSelectedScheduleVal() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnApply(_ sender: Any) {
        let window = UIApplication.shared.keyWindow
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        let viewPopUp = ScheduleAlertView(frame: frame)
        viewPopUp.btnNo .setTitle("ok", for: .normal)
        viewPopUp.btnYes.isHidden = true
        viewPopUp.imgType.image = UIImage (named: "icDisconnect72")
        viewPopUp.lblAlert.text = "Failed to apply the schedule. Please try again."
        viewPopUp.quitScheduleDelagate = self
        window?.addSubview(viewPopUp)
    }
    
    
}

//MARK:- UITableViewDelegate, UITableViewDataSource
extension CreateScheduleViewController: UITableViewDelegate, UITableViewDataSource, selectIntensityTypeProtocol, selectRepeatDaysProtocol, changeTimeProtocol, changeDurationProtocol {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let identifier = "StartTimeTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! StartTimeTableViewCell
            cell.selectionStyle = .none
            cell.timeChangeDelegate = self
            return cell
        }else if indexPath.row == 1 {
            let identifier = "DurationTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! DurationTableViewCell
            cell.selectionStyle = .none
            cell.changeDurationDelegate = self
            return cell
        }else if indexPath.row == 2 {
            let identifier = "IntensityTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! IntensityTableViewCell
            cell.selectIntensityDelegate = self
            cell.selectionStyle = .none
            return cell
        }else {
            let identifier = "RepeatTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! RepeatTableViewCell
            cell.selectedRepeatDelegate = self
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let control = self.storyboard?.instantiateViewController(withIdentifier: "DeviceSettingViewController") as! DeviceSettingViewController
            self.present(control, animated: true, completion: nil)
        }else if indexPath.row == 1 {
            
        }else if indexPath.row == 2 {
            
        }else {

        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }else if indexPath.row == 1 {
            return UITableViewAutomaticDimension
        }else if indexPath.row == 2 {
            
            return 160
        }else {
            
            return 242
        }
    }
    
    
    // Select intensity type delegate method
    func selectIntensityWithType(type: intensityType) {
        switch type {
        case intensityType.low:
            isSelectSchedule = true
            break
        case intensityType.medium:
            isSelectSchedule = true
            break
            
        case intensityType.high:
            isSelectSchedule = true
            break
        default:
            print("Nothing to do")
            break
        }
    }
    
    func selectIntensityInfo() {
        let window = UIApplication.shared.keyWindow
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        let viewPopUp = IntensityInfoView(frame: frame)
        window?.addSubview(viewPopUp)
    }
    
    // Select repeat selected days method
    func selectRepeatDaysWith(arr: NSMutableArray) {
        if arr.count > 0 {
            isSelectSchedule = true
        }
    }
    
    // Select time from the picker view
    func changeTimeWith(time: String) {
        if time != "" {
            isSelectSchedule = true
        }
    }
    
    // Select duration from the picker view
    func changeDurationWith(duration: String) {
        if duration != "" {
            isSelectSchedule = true
        }
    }
    
}
