//
//  IntensityTableViewCell.swift
//  AromeoDiffuser
//
//  Created by vinove on 13/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

enum intensityType {
    case low
    case medium
    case high
}

protocol selectIntensityTypeProtocol {
    func selectIntensityWithType(type: intensityType)
    func selectIntensityInfo()
}

class IntensityTableViewCell: UITableViewCell {

    @IBOutlet var arrIntensity: [UIButton]!
    var selectIntensityDelegate: selectIntensityTypeProtocol?
    var type: intensityType?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnIntensityInfo(_ sender: Any) {
        if selectIntensityDelegate != nil {
            selectIntensityDelegate?.selectIntensityInfo()
        }
    }
    
    
    @IBAction func btnIntensity(_ sender: UIButton) {
        self.deselectAllIntensity()
        switch sender.tag {
        case 0:
            type = intensityType.low
            sender.setImage(UIImage (named: "icLowSelected"), for: .normal)
            break
        case 1:
            type = intensityType.medium
            sender.setImage(UIImage (named: "icMediumSelected"), for: .normal)
            break
        case 2:
            type = intensityType.high
            sender.setImage(UIImage (named: "icHighSelected"), for: .normal)
            break
        default:
            break
        }
        if selectIntensityDelegate != nil {
            selectIntensityDelegate?.selectIntensityWithType(type: type!)
        }
    }
    
    func deselectAllIntensity() {
        for btn in arrIntensity {
            let btnAllIntensity: UIButton = btn
            switch btn.tag {
            case 0:
                btnAllIntensity.setImage(UIImage (named: "icLowDefault"), for: .normal)
                break
            case 1:
                btnAllIntensity.setImage(UIImage (named: "icMediumDefault"), for: .normal)
                break
            case 2:
                btnAllIntensity.setImage(UIImage (named: "icHighDefault"), for: .normal)
                break
            default:
                break
            }
        }
    }
}
