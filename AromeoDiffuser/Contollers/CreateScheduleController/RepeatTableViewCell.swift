//
//  RepeatTableViewCell.swift
//  AromeoDiffuser
//
//  Created by vinove on 13/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

protocol selectRepeatDaysProtocol {
    func selectRepeatDaysWith(arr: NSMutableArray)
}

class RepeatTableViewCell: UITableViewCell {

    @IBOutlet var arrDays: [UIButton]!
    var arrSeletedDays = NSMutableArray()
    var selectedRepeatDelegate:selectRepeatDaysProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnSelectDay(_ sender: UIButton) {
        //self.deselectAllRepeat()
        if sender.isSelected {
            sender.setBackgroundImage(UIImage (named: "icScheduleDefault"), for: .normal)
        }else {
            sender.setBackgroundImage(UIImage (named: "icScheduleSelected"), for: .normal)
        }
        
        var isSelected:Bool = false
        var removeIndex: Int = 0
        if arrSeletedDays.count > 0 {
            for i in 0 ..< arrSeletedDays.count {
                let selectedVal: Int = arrSeletedDays[i] as! Int
                if sender.tag == selectedVal {
                    isSelected = true
                    removeIndex = i
                    sender.setBackgroundImage(UIImage (named: "icScheduleDefault"), for: .normal)
                }
            }
            if isSelected {
                arrSeletedDays.removeObject(at: removeIndex)
            }else {
                arrSeletedDays.add(sender.tag)
            }
        }else {
            arrSeletedDays.add(sender.tag)
        }

        print(arrSeletedDays)
        
        if selectedRepeatDelegate != nil {
            selectedRepeatDelegate?.selectRepeatDaysWith(arr: arrSeletedDays)
        }
    }
    
}
