//
//  ManageScheduleViewController.swift
//  AromeoDiffuser
//
//  Created by vinove on 14/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class ManageScheduleViewController: DiffuserViewController {

    @IBOutlet weak var scheduleTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scheduleTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnAddSchedule(_ sender: Any) {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "CreateScheduleViewController") as! CreateScheduleViewController
        self.present(control, animated: true, completion: nil)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension ManageScheduleViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "ManageScheduleTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ManageScheduleTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 118
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let applyAction = UITableViewRowAction(style: .normal, title: "Apply") { (rowAction, indexPath) in
            //TODO: Delete the row at indexPath here
        }
        applyAction.backgroundColor = UIColor (red: 111.0/255.0, green: 128.0/255.0, blue: 196.0/255.0, alpha: 1)
        return [applyAction]
    }
}
