//
//  ManageScheduleTableViewCell.swift
//  AromeoDiffuser
//
//  Created by vinove on 14/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class ManageScheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.setShadow(toRemove: false)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
