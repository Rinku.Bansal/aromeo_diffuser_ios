//
//  StartTimeTableViewCell.swift
//  AromeoDiffuser
//
//  Created by vinove on 13/11/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

protocol changeTimeProtocol {
    func changeTimeWith(time: String)
}

class StartTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBOutlet weak var dateContainerView: UIView!
    var timeChangeDelegate: changeTimeProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        let dateString = dateFormatter.string(from: Date())
        lblTime.text = "\(dateString)"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func datePickerChangeValue(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        let dateString = dateFormatter.string(from: sender.date)
        lblTime.text = "\(dateString)"
        
        if timeChangeDelegate != nil {
            timeChangeDelegate?.changeTimeWith(time: dateString)
        }
    }

}
