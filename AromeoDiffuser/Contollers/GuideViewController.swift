//
//  GuideViewController.swift
//  AromeoDiffuser
//
//  Created by Prashant on 11/10/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import UIKit

class GuideViewController: DiffuserViewController {

    @IBOutlet weak var viewNav: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Handle Btn Events
    @IBAction func handleBtnBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnInfo(_ sender: Any) {
        let control = self.storyboard?.instantiateViewController(withIdentifier: "ManageScheduleViewController") as! ManageScheduleViewController
        self.present(control, animated: true, completion: nil)
    }
    
}

/*
extension GuideViewController : UIScrollViewDelegate {
    
    func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        ///self.viewNav.backgroundColor = .green
    }
    /*
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (0 < scrollView.contentOffset.y) {
            // moved to top
            print("move Up\(scrollView.contentOffset.y)")
            print("alpha\(Float(scrollView.contentOffset.y / 166))")
            navigationController?.navigationBar.alpha = scrollView.contentOffset.y / 166
            if Float(scrollView.contentOffset.y / 166) >= 1.0 {
                
//                self.viewNav.backgroundColor = UIColor.rgb(red: 251, green: 250, blue: 255, alpha: 0.8)
                
            }
            
        } else if (0 > scrollView.contentOffset.y) {
            // moved to bottom
            
//            self.viewNav.backgroundColor = UIColor.rgb(red: 251, green: 250, blue: 255, alpha: scrollView.contentOffset.y / 166)
            print("alpha\(Float(scrollView.contentOffset.y / 166))")
            print("move down\(scrollView.contentOffset.y)")
            
        } else {
            
            // didn't move
        }
    }
    */
}
*/










