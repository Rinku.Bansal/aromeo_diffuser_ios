//
//  UUIDKey.swift
//  Basic Chat
//
//  Created by Trevor Beaton on 12/3/16.
//  Copyright © 2016 Vanguard Logic LLC. All rights reserved.
//

import CoreBluetooth
//Uart Service uuid

/*
 - 0 : <CBCharacteristic: 0x1c40b8ba0, UUID = EE5F11F1-BD3A-4A3C-81FF-10B8D475F302, properties = 0xA, value = (null), notifying = NO>  ---->  enable
 - 1 : <CBCharacteristic: 0x1c02a0ba0, UUID = 34DC1C36-2E36-4AA5-A480-5D0929A6BD47, properties = 0xA, value = (null), notifying = NO> -----> INTENSITY
 - 2 : <CBCharacteristic: 0x1c02a0d20, UUID = ED96B36B-F5A2-4131-8EE3-87E02BF8DF85, properties = 0xA, value = (null), notifying = NO>
 - 3 : <CBCharacteristic: 0x1c40b8c00, UUID = 9BDCB426-E3CA-4EE8-B612-39F556867264, properties = 0xA, value = (null), notifying = NO> -----> SCHEDULE
 - 4 : <CBCharacteristic: 0x1c40b8d80, UUID = BFEBC03C-B717-4DF1-B294-D1DEC74BF9EC, properties = 0x2, value = (null), notifying = NO> ------> STATE
 - 5 : <CBCharacteristic: 0x1c40bc260, UUID = 285252D1-47D2-4D6A-ACBD-194CA3670B9F, properties = 0xA, value = (null), notifying = NO> ------> SYNCHRONIZE

 */


let kBLEService_UUID                        = "1d85df88-8b57-4c49-89e2-fa80e020c04e" /* "25AC4332-F51F-6E08-3468-B0FF96E44549"*/

let kBLE_Characteristic_uuid_Enable         = "EE5F11F1-BD3A-4A3C-81FF-10B8D475F302"
let kBLE_Characteristic_uuid_Intensity      = "34DC1C36-2E36-4AA5-A480-5D0929A6BD47"
let kBLE_Characteristic_uuid_Schedule       = "9BDCB426-E3CA-4EE8-B612-39F556867264"
let kBLE_Characteristic_uuid_State          = "BFEBC03C-B717-4DF1-B294-D1DEC74BF9EC"
let kBLE_Characteristic_uuid_Syncronize     = "285252D1-47D2-4D6A-ACBD-194CA3670B9F"
let MaxCharacters = 20

let BLEService_UUID                     = CBUUID(string: kBLEService_UUID)
let BLE_Characteristic_uuid_Enable      = CBUUID(string: kBLE_Characteristic_uuid_Enable)//(Property = Write without response)
let BLE_Characteristic_uuid_Intensity   = CBUUID(string: kBLE_Characteristic_uuid_Intensity)// (Property = Read/Notify)
let BLE_Characteristic_uuid_Schedule    = CBUUID(string: kBLE_Characteristic_uuid_Schedule)
let BLE_Characteristic_uuid_State       = CBUUID(string: kBLE_Characteristic_uuid_State)
let BLE_Characteristic_uuid_Syncronize  = CBUUID(string: kBLE_Characteristic_uuid_Syncronize)

