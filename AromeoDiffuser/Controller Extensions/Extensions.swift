//
//  Extensions.swift
//  AromeoDiffuser
//
//  Created by Prashant on 12/10/18.
//  Copyright © 2018 Vinove. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ScanningViewController

extension ScanningViewController:  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    // MARK : - Collection view delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrGuide.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "GuideCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! GuideCell
        
        let dict = arrGuide[indexPath.row]
        
        cell.img.image = UIImage(named: dict.keys.first!)
        cell.lbl.text = dict[dict.keys.first!]
        
        if indexPath.item == 0 {
            //cell.lbl.backgroundColor = UIColor.gray
        }
        else {
            //cell.lbl.backgroundColor = UIColor.green
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height:CGFloat = 320 //collectionView.frame.height
        return CGSize(width: collectionView.frame.width,height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView is UICollectionView {
            var visibleRect = CGRect()
            visibleRect.origin = self.collectionviewGuide.contentOffset
            visibleRect.size = self.collectionviewGuide.bounds.size
            let visiblePoint = CGPoint(x: CGFloat(visibleRect.midX), y: CGFloat(visibleRect.midY))
            let visibleIndexPath: IndexPath? = self.collectionviewGuide.indexPathForItem(at: visiblePoint)
            
            if  let indexPath = visibleIndexPath{
                print("Visible cell's index is : \(indexPath.row)")
                
                self.pagecontrol.currentPage = indexPath.row
                
            }
        }
    }
}



extension ScanningViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peripherals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "AromeoCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! AromeoCell
        //cell.device = peripherals[indexPath.row]
        cell.setData(device: peripherals[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54 + 25
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.peripherals.count <= 0 {
            return
        }
        
        let device: Device? = peripherals[indexPath.row]
        if let peripheral =  device?.peripheral {
            
            if peripheral.name == "MyESP32" || peripheral.identifier.uuidString == kBLEService_UUID || peripheral.identifier.uuidString == "25AC4332-F51F-6E08-3468-B0FF96E44549" {
               
                self.connect(peripheral)
            }
            else {
                 //self.connect(peripheral)
                 Utility.main.showAlert(title: nil, message: "Device not supported", controller: self)
            }
            
            //let cell = tableView.cellForRow(at: indexPath) as! AromeoCell
            //cell.viewBack.backgroundColor = UIColor.rgb(red: 111, green: 128, blue: 196)
            
        }else {
            if isBluetoothStatus {
                self.startScan()
            }else {
                print("Nothing to connect")
                self.showBluetoothAlert()
            }
        }
    }
    
    
}
